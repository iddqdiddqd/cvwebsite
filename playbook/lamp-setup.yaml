---
- name: LAMP Setup
  hosts: all
  become: true
  become_method: sudo
  handlers:
    - name: Restart Nginx
      service:
        name: nginx
        state: restarted

  tasks:

# APT tasks
    - name: Update and upgrade apt packages
      apt:
        upgrade: yes
        update_cache: yes

    - name: Install packages
      apt:
        name:
          - nginx
          - nginx-extras
        state: present

# Nginx HTTP configuration
    - name: Create document root
      file:
        path: "/var/www/{{ lookup('env', 'HTTP_HOST') }}"
        state: directory
        owner: "www-data"
        group: "www-data"
        mode: '0755'

# Nginx HTTPS configuration
    - name: Set Nginx SSL conf file
      template:
        src: "conf/nginx_https.conf.j2"
        dest: "/etc/nginx/sites-available/{{ lookup('env', 'HTTP_CONF') }}"
      notify: Restart Nginx

    - name: Enable new site
      file:
        src: "/etc/nginx/sites-available/{{ lookup('env', 'HTTP_CONF') }}"
        dest: "/etc/nginx/sites-enabled/{{ lookup('env', 'HTTP_CONF') }}"
        state: link

    - name: Remove default site
      file:
        path: "/etc/nginx/sites-enabled/default"
        state: absent

# SSL configuration
    - name: Download acme.sh
      become_user: "{{ lookup('env', 'DEFAULT_USER') }}"
      shell: /usr/bin/curl https://get.acme.sh | /usr/bin/sh -s email={{ lookup('env', 'SSL_ADMIN_EMAIL') }}

    - name: Create SSL renewal cron
      template:
        src: "conf/ssl_update.sh.j2"
        dest: "/etc/cron.weekly/ssl_update"
        mode: 0755

    - name: Export acme.sh env vars
      become_user: "{{ lookup('env', 'DEFAULT_USER') }}"
      shell: "export CF_Key=${CLOUDFLARE_API_KEY} && export CF_Email=${CLOUDFLARE_EMAIL}"

    - name: Sign the CSR
      become_user: "{{ lookup('env', 'DEFAULT_USER') }}"
      shell: "curl -I https://{{ lookup('env', 'HTTP_HOST') }} &>/dev/null ; [[ $? -ne 0 ]] && bash /etc/cron.weekly/ssl_update || echo 'No SSL installation is required now.'"

# Publish the document root
    - name: Publish document root
      copy:
        src: "public_html/"
        dest: "/var/www/{{ lookup('env', 'HTTP_HOST') }}/"
        owner: www-data
        group: www-data
        mode: 0644

# Mask Nginx version from headers
    - name: Update nginx.conf
      become_user: "{{ lookup('env', 'DEFAULT_USER') }}"
      shell: sudo sed -i "s|# server_tokens off|server_tokens off|g" /etc/nginx/nginx.conf
      notify: Restart Nginx

# Update locale
    - name: Update locale
      become_user: "{{ lookup('env', 'DEFAULT_USER') }}"
      shell: sudo unlink /etc/localtime && sudo ln -s /usr/share/zoneinfo/Etc/GMT-3 /etc/localtime
