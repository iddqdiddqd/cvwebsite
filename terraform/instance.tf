resource "aws_security_group" "cv-website" {
  name                            = lookup(var.awsprops, "secgroupname")
  description                     = lookup(var.awsprops, "secgroupname")
  vpc_id                          = lookup(var.awsprops, "vpc")

  ingress {
    from_port                     = 22
    protocol                      = "tcp"
    to_port                       = 22
    cidr_blocks                   = ["0.0.0.0/0"]
  }

  ingress {
    from_port                     = 80
    protocol                      = "tcp"
    to_port                       = 80
    cidr_blocks                   = ["0.0.0.0/0"]
  }

  ingress {
    from_port                     = 443
    protocol                      = "tcp"
    to_port                       = 443
    cidr_blocks                   = ["0.0.0.0/0"]
  }

  egress {
    from_port                     = 0
    to_port                       = 0
    protocol                      = "-1"
    cidr_blocks                   = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy         = true
  }

}

resource "aws_key_pair" "cv-website-keypair" {
  key_name                        = "cv-website-keypair"
  public_key                      = var.ec2_public_key
}

resource "aws_instance" "cv-website" {
  ami                             = lookup(var.awsprops, "ami")
  instance_type                   = lookup(var.awsprops, "itype")
  subnet_id                       = lookup(var.awsprops, "subnet")
  associate_public_ip_address     = lookup(var.awsprops, "publicip")
  key_name                        = lookup(var.awsprops, "keyname")

  vpc_security_group_ids = [
    aws_security_group.cv-website.id
  ]

  tags = {
    Name                          = "CV Website"
    OS                            = "UBUNTU"
    Provision                     = "IAC"
  }

  depends_on = [
    aws_security_group.cv-website,
    aws_key_pair.cv-website-keypair
  ]
}
