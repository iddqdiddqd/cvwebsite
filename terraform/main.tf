terraform {

 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 3.0"
   }
 }

 backend "s3" {
    bucket = "tf-backend-cv-website"
    key    = "terraform.tfstate"
    region = "us-east-1"
    }
}

provider "aws" {
 region = "us-east-1"
 access_key = var.aws_access_key
 secret_key = var.aws_secret_key
}
