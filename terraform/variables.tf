variable "awsprops" {
    type = map
    default = {
      secgroupname = "cv-website-security-group"
      vpc = "vpc-7bc36001"
      ami = "ami-04505e74c0741db8d"
      itype = "t2.micro"
      subnet = "subnet-7352c22f"
      publicip = true
      keyname = "cv-website-keypair"
    }
}

variable "aws_access_key" {
  type = string
  default = "changeme"
}

variable "aws_secret_key" {
  type = string
  default = "changeme"
}

variable "ec2_public_key" {
  type = string
  default = "changeme"
}
