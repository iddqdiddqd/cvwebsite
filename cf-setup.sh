#!/usr/bin/env bash

set -x

# Enable SSL full strict mode
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/ssl" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '{"value":"strict"}'

# Enable HSTS
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/security_header" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '{"value":{"strict_transport_security":{"enabled":true,"max_age":86400,"include_subdomains":true,"nosniff":true}}}'

# Set TLS 1.2 as minimum TLS version
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/min_tls_version" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '{"value":"1.2"}'

# Enable TLS 1.3
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/tls_1_3" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '{"value":"on"}'

# Enable automatic HTTPS rewrites
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/automatic_https_rewrites" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '{"value":"on"}'

# Add AFW filter
curl -X POST "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/filters" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  --data '[{"id":"Aa9xHzH6un3UdoVZKC5ZTiClQtDLelIG","expression":"(ip.geoip.country eq \"RU\") or (ip.geoip.country eq \"BY\")","paused":false,"description":"rissua-sosatb","ref":"rissua-sosatb"}]'

RULE_ID=$(curl -X GET "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/filters" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" | grep -B2 'rissua-sosatb' | head -1 | cut -d'"' -f4)

# Create WAF rule based on the filter
echo '[
   {
      "id":"Aa9xHzH6un3UdoVZKC5ZTiClQtDLelIG",
      "action":"block",
      "priority":50,
      "paused":false,
      "description":"Blocks traffic identified during investigation for MIR-31",
      "ref":"MIR-31",
      "filter":{
         "id":"Aa9xHzH6un3UdoVZKC5ZTiClQtDLelIG",
         "expression":"(ip.geoip.country eq \"RU\") or (ip.geoip.country eq \"BY\")",
         "paused":false,
         "description":"rissua-sosatb",
         "ref":"rissua-sosatb"
      }
   }
]' > waf-rule.json

sed -i "s|Aa9xHzH6un3UdoVZKC5ZTiClQtDLelIG|${RULE_ID}|g" waf-rule.json

curl -X POST "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/firewall/rules" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type:application/json" \
  -d @waf-rule.json

rm waf-rule.json

# Enable always use HTTPS
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/always_use_https" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type: application/json" \
  --data '{"value":"on"}'

# Turn off opportunistic encryption
curl -X PATCH "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/settings/opportunistic_encryption" \
  -H "X-Auth-Email:${CLOUDFLARE_EMAIL}" \
  -H "X-Auth-Key:${CLOUDFLARE_API_KEY}" \
  -H "Content-Type: application/json" \
  --data '{"value":"off"}'
